## **Introduction to Data Warehousing Assignment**
### **Authors**
- [**Patrik Ekman**](https://gitlab.com/patek624)
- [**Jonas Jonsson**](https://gitlab.com/brorjonas)
- [**Maja Bernau**](https://gitlab.com/maja.bernau)
### **Preparations**
#### Way of working
* **Visual Studio Code** with **Live Share** for collaborative mob programming
* **Jupyter Notebook** to document the ETL process, with screenshots and descriptions of each step
#### Create source and destination databases


```python
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

db_params = {
    "host": "localhost",
    "user": "postgres",
    "password": "password"
}

connection = psycopg2.connect(**db_params)
connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
cursor = connection.cursor()

cursor.execute("CREATE DATABASE source_db;")
cursor.execute("CREATE DATABASE destination_db;")

cursor.close()
connection.close()
```

#### Load SQL functionality into the notebook and point towards the source database
* Specify *username*, *password* and *database name* in connection string


```python
# Install required packages (run this only once)
# !pip install ipython-sql psycopg2-binary

%load_ext sql
%sql postgresql://postgres:password@localhost:5432/source_db
```

## **Tasks**
### **Database Exploration**
* *Examine the provided SQL file to understand the structure and relationships of the coffeemerchant database.*
* *Identify the tables and fields that will populate the ```dim_time```, ```dim_customer```, ```dim_inventory```, and ```fact_sales``` tables in your data warehouse.*

![](source_db_schema.png)

#### Conceptual design - identification of fields that will populate the dimension tables

![](star_schema_concept.png)

#### Populate source database with initial data by reading `coffee_merchant.sql` and executing the commands in it


```python
file_path = './coffee_merchant.sql'
with open(file_path, 'r') as file:
    sql_commands = file.read() 

%sql $sql_commands
```

     * postgresql://postgres:***@localhost:5432/source_db
    Done.
    51 rows affected.
    Done.
    1587 rows affected.
    Done.
    256 rows affected.
    Done.
    22 rows affected.
    Done.
    128 rows affected.
    Done.
    500 rows affected.
    Done.
    2192 rows affected.
    []



#### Validation of source database fields that will populate the dimension tables


```sql
%%sql
SELECT 
  CASE 
    WHEN COUNT(*) = COUNT(CASE WHEN price > 0 THEN 1 END) THEN 'All price column values are positive'
    ELSE 'Contains negative price column values'
  END
FROM orderlines;
```

     * postgresql://postgres:***@localhost:5432/source_db
    1 rows affected.
    




<table>
    <thead>
        <tr>
            <th>case</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>All price column values are positive</td>
        </tr>
    </tbody>
</table>




```sql
%%sql
SELECT 
  CASE 
    WHEN COUNT(*) = COUNT(CASE WHEN quantity > 0 THEN 1 END) THEN 'All quantity column values are positive'
    ELSE 'Contains negative quantity column values'
  END
FROM orderlines;
```

     * postgresql://postgres:***@localhost:5432/source_db
    1 rows affected.
    




<table>
    <thead>
        <tr>
            <th>case</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>All quantity column values are positive</td>
        </tr>
    </tbody>
</table>




```sql
%%sql
SELECT 
  CASE 
    WHEN COUNT(*) = COUNT(CASE WHEN discount <= 1 AND discount >= 0 THEN 1 END) THEN 'All discount values are between 0 and 1'
    ELSE 'Contains negative discount values'
  END
FROM orderlines;
```

     * postgresql://postgres:***@localhost:5432/source_db
    1 rows affected.
    




<table>
    <thead>
        <tr>
            <th>case</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>All discount values are between 0 and 1</td>
        </tr>
    </tbody>
</table>




```sql
%%sql
SELECT 
  CASE 
    WHEN COUNT(*) = COUNT(CASE WHEN ItemType LIKE 'C' OR ItemType LIKE 'T' THEN 1 END) THEN 'Only Tea and Coffee products in data.'
    ELSE 'Found other products then Tea and Coffee.'
  END
FROM inventory;
```

     * postgresql://postgres:***@localhost:5432/source_db
    1 rows affected.
    




<table>
    <thead>
        <tr>
            <th>case</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Only Tea and Coffee products in data.</td>
        </tr>
    </tbody>
</table>



### **ETL Process**
* *Use an ETL tool to create ETL jobs that extract data from the source database.*
* *Transform the data according to the requirements of the data warehouse schema.*
* *Load the transformed data into the PostgreSQL database.*

#### Extract and transform data from the source database and store as CSV files


```sql
%%sql

-- save inventory data
Copy (
    SELECT 
        i.InventoryID, 
        i.Name, 
        i.ItemType, 
        i.Price, 
        c.CountryName
    FROM 
        countries c
    RIGHT JOIN inventory i ON i.CountryID = c.CountryID
) To 'c:/users/public/inventory.csv' With CSV DELIMITER ',' HEADER;

-- save customer data
Copy (
    SELECT 
        co.consumerid, 
        co.firstname || ' ' || co.lastname AS name,
        co.city || ' ' || co.state  || ' ' || co.zipcode AS address,
        co.phone
    FROM 
        consumers co
) To 'c:/users/public/customer.csv' With CSV DELIMITER ',' HEADER;

-- save sales data
Copy (
    SELECT
        ol.InventoryID,
        o.ConsumerID,
        o.OrderDate,
        ol.quantity,
        ROUND(ol.quantity * (ol.price * (1 - ol.discount))) AS total_sold,
        e.employeeid,
        e.commissionrate
    FROM 
        orders o
    INNER JOIN 
        orderlines ol USING (orderid)
    INNER JOIN 
        employees e USING (employeeid)
) To 'c:/users/public/sales.csv' With CSV DELIMITER ',' HEADER;

```

     * postgresql://postgres:***@localhost:5432/source_db
    128 rows affected.
    1587 rows affected.
    2192 rows affected.
    []



#### Create tables in destination database
##### Physical design of dimension tables
![](star_schema_physical_design.drawio.svg)
##### Point to destination database and create dimension tables


```sql
%%sql
postgresql://postgres:password@localhost:5432/destination_db

DROP TABLE IF EXISTS fact_sales;
DROP TABLE IF EXISTS dim_inventory;
DROP TABLE IF EXISTS dim_customer;
DROP TABLE IF EXISTS dim_time;

CREATE TABLE dim_inventory (
    inventory_id SERIAL PRIMARY KEY,
    name VARCHAR(50),
    category VARCHAR(1),
    price decimal(10,2),
    country_name VARCHAR(50)
);

CREATE TABLE dim_customer (
    customer_id SERIAL PRIMARY KEY,
    name VARCHAR(50),
    address VARCHAR(255),
    phone VARCHAR(25)
);

CREATE TABLE dim_time (
    date_id DATE PRIMARY KEY,
    year INT,
    month INT,
    date INT,
    day_of_week INT
);

CREATE TABLE fact_sales (
    sale_id SERIAL PRIMARY KEY,
    inventory_id INT REFERENCES dim_inventory(inventory_id),
    customer_id INT REFERENCES dim_customer(customer_id),
    date_id DATE REFERENCES dim_time(date_id),
    quantity INT NOT NULL,
    total_sold DECIMAL(10,2) NOT NULL,
    employee_id INT NOT NULL, 
    commission_rate DECIMAL(10,2) NOT NULL
);

```

    Done.
    Done.
    Done.
    Done.
    Done.
    Done.
    Done.
    Done.
    []


#### Load data into the dimension tables of the destination database


```sql
%%sql

INSERT INTO dim_time (
    SELECT 
    date, 
    EXTRACT (year FROM date) AS year, 
    EXTRACT (month FROM date) AS month, 
    EXTRACT (day FROM date) as date,
    EXTRACT (isodow FROM date) as day_of_week
    FROM (
        SELECT 
            generate_series(
                '2005-10-01'::timestamp, 
                Current_Date::timestamp, 
                '1 day'::interval
            ) as date) AS date_stuff
);

SELECT * FROM dim_time LIMIT 5;
```

     * postgresql://postgres:***@localhost:5432/destination_db
       postgresql://postgres:***@localhost:5432/source_db
    6760 rows affected.
    5 rows affected.
    




<table>
    <thead>
        <tr>
            <th>date_id</th>
            <th>year</th>
            <th>month</th>
            <th>date</th>
            <th>day_of_week</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>2005-10-01</td>
            <td>2005</td>
            <td>10</td>
            <td>1</td>
            <td>6</td>
        </tr>
        <tr>
            <td>2005-10-02</td>
            <td>2005</td>
            <td>10</td>
            <td>2</td>
            <td>7</td>
        </tr>
        <tr>
            <td>2005-10-03</td>
            <td>2005</td>
            <td>10</td>
            <td>3</td>
            <td>1</td>
        </tr>
        <tr>
            <td>2005-10-04</td>
            <td>2005</td>
            <td>10</td>
            <td>4</td>
            <td>2</td>
        </tr>
        <tr>
            <td>2005-10-05</td>
            <td>2005</td>
            <td>10</td>
            <td>5</td>
            <td>3</td>
        </tr>
    </tbody>
</table>




```sql
%%sql
COPY dim_inventory(inventory_id, Name, category, Price, Country_Name)
FROM 'c:/users/public/inventory.csv'
DELIMITER ',' CSV HEADER;

SELECT * FROM dim_inventory LIMIT 5;
```

     * postgresql://postgres:***@localhost:5432/destination_db
       postgresql://postgres:***@localhost:5432/source_db
    128 rows affected.
    5 rows affected.
    




<table>
    <thead>
        <tr>
            <th>inventory_id</th>
            <th>name</th>
            <th>category</th>
            <th>price</th>
            <th>country_name</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>202</td>
            <td>Bolivia Organic</td>
            <td>C</td>
            <td>12.95</td>
            <td>Bolivia</td>
        </tr>
        <tr>
            <td>344</td>
            <td>Brazil Bourbon Santos</td>
            <td>C</td>
            <td>3.80</td>
            <td>Brazil</td>
        </tr>
        <tr>
            <td>335</td>
            <td>Brazil Sul De Minas Cerra</td>
            <td>C</td>
            <td>7.40</td>
            <td>Brazil</td>
        </tr>
        <tr>
            <td>374</td>
            <td>Queen Mary blend</td>
            <td>T</td>
            <td>8.40</td>
            <td>China</td>
        </tr>
        <tr>
            <td>371</td>
            <td>Lapsang Souchong</td>
            <td>T</td>
            <td>9.10</td>
            <td>China</td>
        </tr>
    </tbody>
</table>




```sql
%%sql

COPY dim_customer(customer_id, Name, address, phone)
FROM 'c:/users/public/customer.csv'
DELIMITER ','
CSV HEADER;

SELECT * FROM dim_customer LIMIT 5;
```

     * postgresql://postgres:***@localhost:5432/destination_db
       postgresql://postgres:***@localhost:5432/source_db
    1587 rows affected.
    5 rows affected.
    




<table>
    <thead>
        <tr>
            <th>customer_id</th>
            <th>name</th>
            <th>address</th>
            <th>phone</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>30121</td>
            <td>F. Stanley Best</td>
            <td>Little Rock AR 72202</td>
            <td>(501) 555-6079</td>
        </tr>
        <tr>
            <td>30125</td>
            <td>Duane A. Maul</td>
            <td>Seguin TX 78155</td>
            <td>(210) 555-1483</td>
        </tr>
        <tr>
            <td>30129</td>
            <td>Alan J. Rigas</td>
            <td>Newark NJ 07114</td>
            <td>(201) 555-1311</td>
        </tr>
        <tr>
            <td>30132</td>
            <td>T. Peter Murray</td>
            <td>Owosso MI 48867</td>
            <td>(517) 555-7364</td>
        </tr>
        <tr>
            <td>30136</td>
            <td>Carl E. Shelton</td>
            <td>Oklahoma City OK 73102</td>
            <td>(405) 555-1168</td>
        </tr>
    </tbody>
</table>




```sql
%%sql

COPY fact_sales(inventory_id, customer_id, date_id, quantity, total_sold, employee_id, commission_rate)
FROM 'c:/users/public/sales.csv'
DELIMITER ','
CSV HEADER;

SELECT * FROM fact_sales LIMIT 5;
```

     * postgresql://postgres:***@localhost:5432/destination_db
       postgresql://postgres:***@localhost:5432/source_db
    2192 rows affected.
    5 rows affected.
    




<table>
    <thead>
        <tr>
            <th>sale_id</th>
            <th>inventory_id</th>
            <th>customer_id</th>
            <th>date_id</th>
            <th>quantity</th>
            <th>total_sold</th>
            <th>employee_id</th>
            <th>commission_rate</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>236</td>
            <td>35222</td>
            <td>2005-10-01</td>
            <td>18</td>
            <td>118.00</td>
            <td>4058</td>
            <td>0.05</td>
        </tr>
        <tr>
            <td>2</td>
            <td>119</td>
            <td>35222</td>
            <td>2005-10-01</td>
            <td>17</td>
            <td>203.00</td>
            <td>4058</td>
            <td>0.05</td>
        </tr>
        <tr>
            <td>3</td>
            <td>188</td>
            <td>35222</td>
            <td>2005-10-01</td>
            <td>17</td>
            <td>66.00</td>
            <td>4058</td>
            <td>0.05</td>
        </tr>
        <tr>
            <td>4</td>
            <td>122</td>
            <td>35222</td>
            <td>2005-10-01</td>
            <td>14</td>
            <td>74.00</td>
            <td>4058</td>
            <td>0.05</td>
        </tr>
        <tr>
            <td>5</td>
            <td>131</td>
            <td>35222</td>
            <td>2005-10-01</td>
            <td>17</td>
            <td>176.00</td>
            <td>4058</td>
            <td>0.05</td>
        </tr>
    </tbody>
</table>



#### Validate against destination database


```python
import psycopg2
import pandas as pd

db_params = {
    "host": "localhost",
    "user": "postgres",
    "password": "password",
    "dbname": "destination_db"
}

connection = psycopg2.connect(**db_params)
cursor = connection.cursor()

cursor.execute(f"SELECT COUNT(*) FROM fact_sales")
db_response = cursor.fetchone()[0]

cursor.close()
connection.close()

results = pd.read_csv('c:/users/public/sales.csv') 

if db_response == len(results):
    print("Matching number of rows")
else: 
    print("Warning - number of rows does not match")
```

    Matching number of rows
    

#### Visualization
![](sales_report.png)

#### Reflections
* Challenges with realtime collaboration and setting up environment locally on all machines.
* The instructions were not clear on who the report was aimed for, we took the decision that it was for top management and tried to figure out what data would be valuable for them. E.g finding top selling employees.
* Hard to determine how to interpret data where the context is not clear. For example, what is the difference between `orderline.price` and `inventory.price`? And can `inventory.onhand` be negative or is that faulty data?

#### Conclusions 
* In reality a quick call with a person at Coffee Merchant to sort out these confusions would have made our work easier!
